@db = CONFIG

drop =  %x[curl -X DELETE "#{@db['protocol']}://#{@db['username']}:#{@db['password']}@#{@db['host']}:#{@db['port']}/#{@db['prefix']}_#{@db['suffix']}"]              

JSON.parse(drop).each do |key, value|
    puts "#{key.to_s.capitalize} : #{value.to_s.capitalize}"
end

drop = %x[curl -X DELETE "#{@db['protocol']}://#{@db['username']}:#{@db['password']}@#{@db['host']}:#{@db['port']}/#{@db['prefix']}_local_#{@db['suffix']}"]

JSON.parse(drop).each do |key, value|
    puts "#{key.to_s.capitalize} : #{value.to_s.capitalize}"
end
