module ApplicationHelper
 
 def district
      return District.find(CONFIG['district_code'])
 end
 
 def app_version
     `git describe`.gsub(/\n/,'')
 end
  
 def record_complete?(child)
  
    complete = false
    
    if child.parents_married_to_each_other == 'Yes'
    
        if child.district_id_number.blank?
				   return complete
				end
				
				if child.first_name.blank?
				   return complete
				end
				
				if child.last_name.blank?
				   return complete
				end
				
				if child.birthdate.blank?
				   return complete
				end
				
				if child.gender.blank?
				   return complete
				end
				
				if child.mother.first_name.blank?
				   return complete
				end
				
				if child.mother.last_name.blank?
				   return complete
				end
				
				if child.father.first_name.blank?
				   return complete
				end
				
				if child.father.last_name.blank?
				   return complete
				end
				
				return true 
		else
		    
		    if child.district_id_number.blank?
				   return complete
				end
		   
				if child.first_name.blank?
				   return complete
				end
				
				if child.last_name.blank?
				   return complete
				end
				
				if child.birthdate.blank?
				   return complete
				end
				
				if child.gender.blank?
				   return complete
				end
				
				if child.mother.first_name.blank?
				   return complete
				end
				
				if child.mother.last_name.blank?
				   return complete
				end
				
				return true 
    end
  
  end
end
